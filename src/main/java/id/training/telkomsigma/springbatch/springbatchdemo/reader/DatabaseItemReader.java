/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.reader;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public class DatabaseItemReader implements ItemReader<Peserta>{

	private DataSource dataSource;
	
	public DatabaseItemReader(DataSource dataSource) {
		this.dataSource = dataSource;
	}
		
	@Override
	public Peserta read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		JdbcPagingItemReader<Peserta> data = new JdbcPagingItemReader<Peserta>();
		data.setDataSource(dataSource);
		data.setPageSize(1);
		data.setRowMapper(new BeanPropertyRowMapper<>(Peserta.class));
		data.setQueryProvider(createQueryProvider());
		return data.read();
	}

	private PagingQueryProvider createQueryProvider() {
		MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();
		queryProvider.setSelectClause("SELECT * ");
		queryProvider.setFromClause("FROM peserta");
		queryProvider.setSortKeys(sortByIdAsc());
		return queryProvider;
	}

	private Map<String, Order> sortByIdAsc() {
		Map<String, Order> sort = new HashMap<>();
		sort.put("id", Order.ASCENDING);
		return sort;
	}
}
