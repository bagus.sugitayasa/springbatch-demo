/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.annotation.OnReadError;
import org.springframework.stereotype.Component;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
public class ItemReaderListener {
	private static final Logger LOG = LoggerFactory.getLogger(ItemReaderListener.class);
	
	@BeforeRead
	public void beforeRead() {
		LOG.info("INTERCEPTOR SEBELUM BACA FILE");
	}
	
	@AfterRead
	public void afterRead(Peserta p) {
		LOG.info("INTERCEPTOR SETELAH BACA FILE {}", p);
	}
	
	@OnReadError
	public void onReadError(Exception e) {
		LOG.error("INTERCEPTOR KETIKA ADA YANG ERROR {}", e);
	}
}
