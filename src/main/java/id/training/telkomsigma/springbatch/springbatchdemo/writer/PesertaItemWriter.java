/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.writer;

import java.sql.SQLDataException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.training.telkomsigma.springbatch.springbatchdemo.dao.PesertaDao;
import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
@StepScope
public class PesertaItemWriter implements ItemWriter<Peserta> {

	private static final Logger LOG = LoggerFactory.getLogger(PesertaItemWriter.class);
	
	@Autowired private PesertaDao pesertaDao;
	
	@Value("#{jobParameters['JobId']}") private String jobId;
	
	@Override
	public void write(List<? extends Peserta> list) throws Exception {
		LOG.info("#Job Param {}",jobId);
		for(Peserta p : list) {
			if(p.getName().equalsIgnoreCase("Name5")){
                throw new SQLDataException("SENGAJA DIBUAT ERROR KETIKA SAVE !!");
            }
            
            LOG.info("PESERTA YANG AKAN DI SAVE : {}",p.getName());
			pesertaDao.save(p);
		}
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	public String getJobId() {
		return jobId;
	}
}
