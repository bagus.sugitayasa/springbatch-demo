/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public interface PesertaDao extends PagingAndSortingRepository<Peserta, String>{

}
