/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data @Entity @Table(name="Peserta")
public class Peserta {
	@Id @GeneratedValue(generator="uuid") @GenericGenerator(name="uuid", strategy="uuid2")
	private String id;
	private String name;
	private String alamat;
	@Temporal(TemporalType.DATE)
	private Date tanggalLahir;
}
