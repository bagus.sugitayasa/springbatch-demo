/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.processor;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
public class PesertaItemProcessor implements ItemProcessor<Peserta, Peserta>{
	@Override
	public Peserta process(Peserta peserta) throws Exception {
		String nama = peserta.getName().toUpperCase();
		Peserta newPeserta = new Peserta();
		newPeserta.setName(nama);
		newPeserta.setAlamat(peserta.getAlamat());
		newPeserta.setTanggalLahir(peserta.getTanggalLahir());
		
		return newPeserta;
	}

}
