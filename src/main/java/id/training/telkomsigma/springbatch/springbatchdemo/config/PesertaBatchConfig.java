/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.config;

import java.sql.SQLDataException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;
import id.training.telkomsigma.springbatch.springbatchdemo.listener.CustomSkipListener;
import id.training.telkomsigma.springbatch.springbatchdemo.listener.ItemReaderListener;
import id.training.telkomsigma.springbatch.springbatchdemo.listener.SkipChekingListener;
import id.training.telkomsigma.springbatch.springbatchdemo.mapper.PesertaMapper;
import id.training.telkomsigma.springbatch.springbatchdemo.processor.PesertaItemProcessor;
import id.training.telkomsigma.springbatch.springbatchdemo.tasklet.ConditionalTasklet;
import id.training.telkomsigma.springbatch.springbatchdemo.tasklet.DeleteFileTasklet;
import id.training.telkomsigma.springbatch.springbatchdemo.tasklet.SampleTasklet;
import id.training.telkomsigma.springbatch.springbatchdemo.writer.PesertaItemWriter;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id:
 */
@Configuration
public class PesertaBatchConfig {
	@Autowired public JdbcTemplate jdbcTemplate;
	@Autowired public JobBuilderFactory jobBuilderFactory;
	@Autowired public StepBuilderFactory stepBuilderFactory;
	@Autowired public JobLauncher jobLauncher;

	/*
	 * Cara 1 menggunakan Autowired, sehingga pada class perlu
	 * ditambahkan @Component
	 */
	@Autowired public PesertaItemProcessor processor;
	@Autowired public PesertaItemWriter itemWriter;
	@Autowired public SkipChekingListener skipChekingListener;
	@Autowired public ItemReaderListener ItemReaderListener;
	@Autowired public CustomSkipListener customSkipListener;

	private static final Logger LOG = LoggerFactory.getLogger(PesertaBatchConfig.class);

	@Bean
	public FlatFileItemReader<Peserta> reader() {
		FlatFileItemReader<Peserta> reader = new FlatFileItemReader<Peserta>();
		reader.setResource(new ClassPathResource("test-data.csv"));
		reader.setLineMapper(new DefaultLineMapper<Peserta>() {
			{
				setLineTokenizer(new DelimitedLineTokenizer() {
					{
						setNames(new String[] { "nama", "alamat", "tanggalLahir" });
					}
				});
				setFieldSetMapper(new PesertaMapper());
			}
		});

		return reader;
	}

	/*
	 * Cara 2 Mendaftarkan sebagai BEAN, sehingga pada class tidak perlu
	 * ditambahkan @Component
	 */
	// @Bean
	// public PesertaItemProcessor processor() {
	// return new PesertaItemProcessor();
	// }

	// @Bean
	// public PesertaItemWriter itemWriter() {
	// return new PesertaItemWriter();
	// }

	// @Bean
	// public SkipChekingListener skipChekingListener() {
	// return new SkipChekingListener();
	// }

	// @Bean
	// public ItemReaderListener ItemReaderListener() {
	// return new ItemReaderListener();
	// }

	// @Bean
	// public CustomSkipListener customSkipListener() {
	// return new CustomSkipListener();
	// }

	@SuppressWarnings("unused")
	//@Scheduled(cron = "*/10 * * * * *")
	public void performJob() {
		try {
			LOG.info("## Job Running at {} ##", new Date());
			JobParameters jParam = new JobParametersBuilder()
					.addString("jobId", String.valueOf(System.currentTimeMillis()))
					.toJobParameters();
			JobExecution execution = jobLauncher.run(importDataPesertaJob(), jParam);
		} catch (Exception e) {
			LOG.error("Error while execute job {}", e.getMessage());
		}
	}

	@Bean
	public Job importDataPesertaJob() {
		/* Paralel Flow */
		Flow flow1 = new FlowBuilder<Flow>("subFlow-1")
				.from(conditionalStep())
				.build();
		
		Flow flow2 = new FlowBuilder<Flow>("subFlow-2")
				.from(sampleStep())
				.build();
		
		return jobBuilderFactory
				.get("importPesertaJob")
				.incrementer(new RunIdIncrementer())
				.flow(importPesertaStep())
				.split(new SimpleAsyncTaskExecutor())
				.add(flow1, flow2)
				.end()
				.build();
		
		/*return jobBuilderFactory
				.get("importPesertaJob")
				.incrementer(new RunIdIncrementer())
				.flow(importPesertaStep())
				.next(deleteFileStep())
				.end()
				.build();*/
		
		/* Conditional Flow */
		/*return jobBuilderFactory
				.get("importPesertaJob")
				.incrementer(new RunIdIncrementer())
				.flow(importPesertaStep())
					.on("Complete With Error")
					.to(conditionalStep())
					.next(deleteFileStep())
				.from(importPesertaStep())
					.on("*")
					.end()
				.end()
				.build();*/	
	}

	@Bean
	public Step importPesertaStep() {
		return stepBuilderFactory.get("step-1")
				.<Peserta, Peserta>chunk(2)
				.reader(reader())
				.processor(processor)
				.writer(itemWriter).faultTolerant() // Untuk toleransi jika ada kesalahan
					.skip(FlatFileParseException.class) // berdasarkan class exception
					.skip(SQLDataException.class) // berdasarkan class exception
					.skipLimit(2) // dengan limit kesalahan sebesar 1 data
					.retry(SQLDataException.class) // retry proses berdasarkan class
					.retryLimit(3) // retry limit execution
				.listener(skipChekingListener) // listener jika terjadi skipLimit dan akan di jalankan exit status complete with error
				.listener(ItemReaderListener) // listener sebagai interceptor
				.listener(customSkipListener) // custom listener
				.build();
	}

	@Bean
	public Step deleteFileStep() {
		return stepBuilderFactory.get("step-2")
				.tasklet(new DeleteFileTasklet())
				.build();
	}
	
	@Bean
	public Step conditionalStep() {
		return stepBuilderFactory.get("step-3")
				.tasklet(new ConditionalTasklet())
				.build();
	}
	
	@Bean
	public Step sampleStep() {
		return stepBuilderFactory.get("step-4")
				.tasklet(new SampleTasklet())
				.build();
	}
}
