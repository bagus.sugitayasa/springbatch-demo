/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;
import org.springframework.stereotype.Component;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
public class CustomSkipListener {
	private static final Logger LOG = LoggerFactory.getLogger(CustomSkipListener.class);
	
	@OnSkipInRead
	public void onSkipInRead(Throwable t) {
		LOG.error("INTERPT ON SKIP IN READER : {}", t.getMessage());
	}
	
	@OnSkipInWrite
    public void onSkipWrite(Peserta p, Throwable t){
        LOG.error("OBJECT YANG ERROR KETIKA DI SAVE : {}",p);
    }
    
    @OnSkipInProcess
    public void onSkipInProcess(Peserta p, Throwable t){
        LOG.error("OBJECT YANG ERROR KETIKA DI ROCESS : {}", p);
    }
}
