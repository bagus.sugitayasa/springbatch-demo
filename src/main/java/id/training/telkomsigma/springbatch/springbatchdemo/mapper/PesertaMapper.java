/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import id.training.telkomsigma.springbatch.springbatchdemo.domain.Peserta;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
public class PesertaMapper implements FieldSetMapper<Peserta>{

	@Override
	public Peserta mapFieldSet(FieldSet fs) throws BindException {
		Peserta peserta = new Peserta();
		peserta.setName(fs.readRawString(0));
		peserta.setAlamat(fs.readString("alamat"));
		peserta.setTanggalLahir(fs.readDate(2));
		
		return peserta;
	}
}
