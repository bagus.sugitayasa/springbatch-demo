/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.tasklet;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Component
public class DeleteFileTasklet implements Tasklet{

	private static final Logger LOG = LoggerFactory.getLogger(DeleteFileTasklet.class);
	
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
		File file = new ClassPathResource("test-data.csv").getFile();
		if (file.delete()) {
			LOG.info("File {} has been deleted!!!", file.getName());
		}
		else {
			LOG.error("Uneble to delete!!!");
		}
		return RepeatStatus.FINISHED;
	}

}
