/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public class SampleTasklet implements Tasklet{

	private static final Logger LOG = LoggerFactory.getLogger(SampleTasklet.class);
	
	@Override
	public RepeatStatus execute(StepContribution paramStepContribution, ChunkContext paramChunkContext)
			throws Exception {
		LOG.info("## Running Tasklet {}", SampleTasklet.class);
		return null;
	}

}
