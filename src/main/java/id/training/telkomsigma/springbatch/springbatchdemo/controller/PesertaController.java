/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.controller;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id:
 */
@RestController
@RequestMapping("/peserta/process")
public class PesertaController {
	@Autowired private JobLauncher jobLauncher;
	@Autowired @Qualifier("importDataPesertaJob") private Job importDataPesertaJob;
	@Autowired @Qualifier("exportDataPesertaJob") private Job exportDataPesertaJob;

	@GetMapping("/run")
	public String executeJob() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		String status = "";
		try {
			JobExecution execution = jobLauncher.run(importDataPesertaJob, new JobParametersBuilder()
					.addString("JobId", String.valueOf(System.currentTimeMillis()))
					.toJobParameters());
			@SuppressWarnings("unused")
			Boolean isFailed = Boolean.FALSE;
			if (BatchStatus.FAILED.equals(execution.getStatus())) {
				isFailed = Boolean.TRUE;
				status = "FAILED!!!";
			} else {
				status = "SUCCESS!!";
			}
		} catch (Exception e) {
			status = "ERROR LAUNCH importDataPesertaFromCsvJob : " + e.getMessage();
		}
		return status;
	}
	
	@GetMapping("/export")
	public String exportJob() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		String status = "";
		try {
			JobExecution execution = jobLauncher.run(exportDataPesertaJob, new JobParametersBuilder()
					.addString("JobId", String.valueOf(System.currentTimeMillis()))
					.toJobParameters());
			@SuppressWarnings("unused")
			Boolean isFailed = Boolean.FALSE;
			if (BatchStatus.FAILED.equals(execution.getStatus())) {
				isFailed = Boolean.TRUE;
				status = "FAILED!!!";
			} else {
				status = "SUCCESS!!";
			}
		} catch (Exception e) {
			status = "ERROR LAUNCH exportDataPesertaJob : " + e.getMessage();
		}
		return status;
	}
}
