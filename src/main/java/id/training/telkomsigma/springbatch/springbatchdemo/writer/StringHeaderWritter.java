/**
 * 
 */
package id.training.telkomsigma.springbatch.springbatchdemo.writer;

import java.io.IOException;
import java.io.Writer;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
public class StringHeaderWritter implements FlatFileHeaderCallback{
	private final String header;
	
	public StringHeaderWritter(String header) {
		this.header = header;
	}

	@Override
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}
}
